﻿using NUnit.Framework;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using System.Text;

namespace Module_3
{
    class Program
    {
        static void Main(string[] args)
        {
            /// Use this method to implement tasks
             Console.WriteLine("Hello world");
        }
    }
    public class Task1
    {
        /// <summary>
        /// Use this method to parse and validate user input
        /// Throw ArgumentException if user input is invalid
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public int ParseAndValidateIntegerNumber(string source)
        {
            try
            {
                return int.Parse(source);
            }
            catch 
            {
                throw new ArgumentException();
            }
        }

        public int Multiplication(int num1, int num2)
        {
            int result =0;
            for (int i = 1; i <= Math.Abs(num2); i++)
            {
                result += num1;
            }
            if (num2 < 0)
                return -result;
            else return result;
        }
    }

    public class Task2
    {
        /// <summary>
        /// Use this method to parse and validate user input
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public bool TryParseNaturalNumber(string input, out int result)
        {
            if (int.TryParse(input, out result) && result >= 0)
                return true;
            else
            {
                Console.WriteLine("Enter natural number");
                input = Console.ReadLine();
                return TryParseNaturalNumber(input, out result);
            }
        }

        public List<int> GetEvenNumbers(int naturalNumber)
        {
            List<int> even = new List<int>();
                for (int i = 0; i < naturalNumber; i++)
                { 
                    even.Add(i*2);
                    Console.WriteLine(even[i]);
                }
            return even;

        }
    }

    public class Task3
    {
        /// <summary>
        /// Use this method to parse and validate user input
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public bool TryParseNaturalNumber(string input, out int result)
        {
            if (int.TryParse(input, out result) && result >= 0)
                return true;
            else
            {
                Console.WriteLine("Enter natural number");
                input = Console.ReadLine();
                return TryParseNaturalNumber(input, out result);
            } 
        }

        public string RemoveDigitFromNumber(int source, int digitToRemove)
        {
                string input = source.ToString();
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < input.Length; i++)
                {
                    if (int.Parse(input[i].ToString()) != digitToRemove)
                        sb.Append(input[i].ToString());
                }
                return sb.ToString();
        }
    }
}
